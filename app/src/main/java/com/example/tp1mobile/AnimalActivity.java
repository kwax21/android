package com.example.tp1mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.tp1mobile.AnimalList.getAnimal;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_char);

        final Bundle extras = getIntent().getExtras();
        final int test = extras.getInt("imge");

        final TextView nom = (TextView) findViewById(R.id.name);
        final ImageView imge = (ImageView) findViewById(R.id.img);
        final TextView esp = (TextView) findViewById(R.id.esp);
        final TextView gest = (TextView) findViewById(R.id.gest);
        final TextView poids = (TextView) findViewById(R.id.poids1);
        final TextView poidss = (TextView) findViewById(R.id.poids2);
        final EditText inpute = (EditText) findViewById(R.id.input);

        final Button bouton = (Button) findViewById(R.id.but);

        nom.setText(extras.getString("nom"));
        imge.setImageResource(extras.getInt("img"));
        esp.setText(extras.getString("esp"));
        gest.setText(extras.getString("gest"));
        poids.setText(extras.getString("poids"));
        poidss.setText(extras.getString("poidss"));
        inpute.setText(extras.getString("inpute"));

        bouton.setClickable(true);
        bouton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String cons = inpute.getText().toString();
                getAnimal(extras.getString("nom")).setConservationStatus(cons);
            }
        });

    }
}
