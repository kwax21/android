package com.example.tp1mobile;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class AnimalAdapter extends RecyclerView.Adapter<AnimalHolder> {

    private String[] namList;
    private Context cont;

    AnimalAdapter(Context conte){
        cont = conte;
        namList = AnimalList.getNameArray();
    }

    @Override
    public AnimalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_recycler, parent, false);
        return new AnimalHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalHolder animalHolder, final int position) {
        Animal current = AnimalList.getAnimal(namList[position]);
        animalHolder.animals.setText(namList[position]);
        animalHolder.logo.setImageResource(current.getImgFile());

        animalHolder.animals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent(cont, AnimalActivity.class);

                Animal current = AnimalList.getAnimal(namList[position]);
                inte.putExtra("nom", namList[position]);
                inte.putExtra("img", current.getImgFile());
                inte.putExtra("esp", current.getStrHightestLifespan());
                inte.putExtra("gest", current.getStrGestationPeriod());
                inte.putExtra("poids", current.getStrBirthWeight());
                inte.putExtra("poidss", current.getStrAdultWeight());
                inte.putExtra("inpute", current.getConservationStatus());

                cont.startActivity(inte);
            }
        });
    }


    @Override
    public int getItemCount() {
        return namList.length;
    }
}

class AnimalHolder extends RecyclerView.ViewHolder {

    TextView animals;
    ImageView logo;
    public AnimalHolder(View itemView) {
        super(itemView);
        animals = (TextView) itemView.findViewById(R.id.anim);
        logo = (ImageView) itemView.findViewById(R.id.logo);
    }
}
